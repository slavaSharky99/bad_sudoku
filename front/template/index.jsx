$(document).ready(function(){
    $('#edit').keypress(validateNumber);
});

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
    	return true;
    }
};

var data;
$.ajax({
  url:"/get_grid",
  type:"GET",
  dataType:"json",
  contentType:"application/json",
  success: function(response) {
    data = response["table"];
    for ( var i = 0; i < Object.keys(response["table"]).length; i++){
      for (var j = 0; j < Object.keys(response["table"][i]).length; j++)
      {
        if (response["table"][i][j] == 0){
          response["table"][i][j] = "";
        }
      }
    }
    class InputForm extends React.Component {
      constructor(props) {
        super(props);
        this.state = {value: ''};
        this.state = {arr: response["table"]}
        this.handleChange = this.handleChange.bind(this);

      }

      handleChange(event) {
        this.state.i = event.target.getAttribute('cust_id')[0];
        this.state.j = event.target.getAttribute('cust_id')[2];

        // console.log(this.state.i,this.state.j);
        this.state.arr[this.state.i][this.state.j] = parseInt(event.target.value,10);
        data[this.state.i][this.state.j] = parseInt(event.target.value,10);
        // console.log(this.state.arr);
        if (this.state.backgroundColor === 'red'){
          this.setState(backgroundColor:'blue');
        }
      }

      render() {
        return (

          this.state.arr.map((row, i) =>
            <tr id={i}>{
            row.map( (val, j) =>
               <td id={[i,j]} className={this.state.backgroundColor}>
                  <input cust_id={[i,j]} type="text" id="edit"
                    defaultValue={val} value={this.state.value}
                    onChange={this.handleChange} maxlength="1" />
               </td>

            )}
            </tr>
          )
        )
      }
    }
    ReactDOM.render(
      <InputForm/>,
      document.getElementById("app")
    )

  },
  error: function(error) {
    console.log(error);
    }
})

$('#send').click(function() {
  console.log(data);
  var flag = true;
  var mass = [];
  for (var i = 0 ; i < Object.keys(data).length; i++){
    for (var j = 0; j < Object.keys(data[i]).length; j++){
      if (data[i][j] == ""){
        flag = false;
        mass.push({'id':[i,j]})
      }
    }
  }
  console.log(mass.length);
  if (flag){
  $.ajax({
    url:"/post_grid",
    type:"POST",
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({grid:data}),
    success: function (response) {
        console.log(response);
        if (response["status"] == "Ok"){
          alert("You win!");
        }
        else{
          alert("You have a mistake");
        }
    },
    error: function(error){
      console.log(error);
    }
  });
  }
  else{
    alert("Empty val");
  }
})
