"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

$(document).ready(function () {
  $('#edit').keypress(validateNumber);
});

function validateNumber(event) {
  var key = window.event ? event.keyCode : event.which;
  if (event.keyCode === 8 || event.keyCode === 46) {
    return true;
  } else if (key < 48 || key > 57) {
    return false;
  } else {
    return true;
  }
};

var data;
$.ajax({
  url: "/get_grid",
  type: "GET",
  dataType: "json",
  contentType: "application/json",
  success: function success(response) {
    data = response["table"];
    for (var i = 0; i < Object.keys(response["table"]).length; i++) {
      for (var j = 0; j < Object.keys(response["table"][i]).length; j++) {
        if (response["table"][i][j] == 0) {
          response["table"][i][j] = "";
        }
      }
    }

    var InputForm = function (_React$Component) {
      _inherits(InputForm, _React$Component);

      function InputForm(props) {
        _classCallCheck(this, InputForm);

        var _this = _possibleConstructorReturn(this, (InputForm.__proto__ || Object.getPrototypeOf(InputForm)).call(this, props));

        _this.state = { value: '' };
        _this.state = { arr: response["table"] };
        _this.handleChange = _this.handleChange.bind(_this);

        return _this;
      }

      _createClass(InputForm, [{
        key: "handleChange",
        value: function handleChange(event) {
          this.state.i = event.target.getAttribute('cust_id')[0];
          this.state.j = event.target.getAttribute('cust_id')[2];

          // console.log(this.state.i,this.state.j);
          this.state.arr[this.state.i][this.state.j] = parseInt(event.target.value, 10);
          data[this.state.i][this.state.j] = parseInt(event.target.value, 10);
          // console.log(this.state.arr);
          if (this.state.backgroundColor === 'red') {
            this.setState(backgroundColor);
          }
        }
      }, {
        key: "render",
        value: function render() {
          var _this2 = this;

          return this.state.arr.map(function (row, i) {
            return React.createElement(
              "tr",
              { id: i },
              row.map(function (val, j) {
                return React.createElement(
                  "td",
                  { id: [i, j], className: _this2.state.backgroundColor },
                  React.createElement("input", { cust_id: [i, j], type: "text", id: "edit",
                    defaultValue: val, value: _this2.state.value,
                    onChange: _this2.handleChange, maxlength: "1" })
                );
              })
            );
          });
        }
      }]);

      return InputForm;
    }(React.Component);

    ReactDOM.render(React.createElement(InputForm, null), document.getElementById("app"));
  },
  error: function error(_error) {
    console.log(_error);
  }
});

$('#send').click(function () {
  console.log(data);
  var flag = true;
  var mass = [];
  for (var i = 0; i < Object.keys(data).length; i++) {
    for (var j = 0; j < Object.keys(data[i]).length; j++) {
      if (data[i][j] == "") {
        flag = false;
        mass.push({ 'id': [i, j] });
      }
    }
  }
  console.log(mass.length);
  if (flag) {
    $.ajax({
      url: "/post_grid",
      type: "POST",
      contentType: 'application/json;charset=UTF-8',
      data: JSON.stringify({ grid: data }),
      success: function success(response) {
        console.log(response);
      },
      error: function error(_error2) {
        console.log(_error2);
      }
    });
  } else {
    alert("Empty val");
  }
});
