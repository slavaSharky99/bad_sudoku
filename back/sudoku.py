import random
from itertools import product

class Grid:

    def __init__(self,n = 3):

        self.n = n
        self.table = [[((i*n + int(i//n) + j) % (n*n) + 1) for j in range(n*n)] for i in range(n*n)]


    def __del__(self):
        pass

    def show(self):
        for i in range(self.n*self.n):
            print (self.table[i])

    def trans(self):

        self.table = list(map(list, zip(*self.table)))


    def swap_rows_small(self):

        area = random.randrange(0,self.n,1)
        line1 = random.randrange(0,self.n,1)

        N1 = area*self.n + line1
        line2 = random.randrange(0,self.n,1)

        while (line1 == line2):
        	line2 = random.randrange(0,self.n,1)

        N2 = area*self.n + line2

        self.table[N1],self.table[N2] = self.table[N2], self.table[N1]


    def swap_colums_small(self):
        Grid.trans(self)
        Grid.swap_rows_small(self)
        Grid.trans(self)


    def swap_rows_area(self):

        area1 = random.randrange(0,self.n,1)

        area2 = random.randrange(0,self.n,1)

        while (area1 == area2):
        	area2 = random.randrange(0,self.n,1)

        for i in range(0, self.n):
            N1 = area1*self.n + i
            N2 = area2*self.n + i
            self.table[N1],self.table[N2] = self.table[N2], self.table[N1]


    def swap_colums_area(self):
        Grid.trans(self)
        Grid.swap_rows_area(self)
        Grid.trans(self)


    def mix(self,amt = 10):

        mix_func = [self.trans,
        			self.swap_rows_small,
        			self.swap_colums_small,
        			self.swap_rows_area,
                    self.swap_colums_area]
        for i in range(1, amt):
        	id_func = random.randrange(0,len(mix_func),1)
        	mix_func[id_func]()


def null_grid(example):
    flook = [[0 for j in range(example.n*example.n)] for i in range(example.n*example.n)]
    iterator = 0
    difficult = example.n ** 4

    example.show()

    while iterator < example.n ** 4:
    	i,j = random.randrange(0, example.n*example.n ,1), random.randrange(0, example.n*example.n ,1)
    	if flook[i][j] == 0:
    		iterator += 1
    		flook[i][j] = 1

    		temp = example.table[i][j]
    		example.table[i][j] = 0
    		difficult -= 1

    		table_solution = []
    		for copy_i in range(0, example.n*example.n):
    			table_solution.append(example.table[copy_i][:])

    		i_solution = 0
    		for solution in check_solve_sudoku((example.n, example.n), table_solution):
    			i_solution += 1

    		if i_solution != 1:
    			example.table[i][j] = temp
    			difficult += 1
    return example

def check_solve_sudoku(size, grid):
    R, C = size
    N = R * C
    X = ([("rc", rc) for rc in product(range(N), range(N))] +
         [("rn", rn) for rn in product(range(N), range(1, N + 1))] +
         [("cn", cn) for cn in product(range(N), range(1, N + 1))] +
         [("bn", bn) for bn in product(range(N), range(1, N + 1))])
    Y = dict()
    for r, c, n in product(range(N), range(N), range(1, N + 1)):
        b = (r // R) * R + (c // C) # Box number
        Y[(r, c, n)] = [
            ("rc", (r, c)),
            ("rn", (r, n)),
            ("cn", (c, n)),
            ("bn", (b, n))]
    X, Y = exact_cover(X, Y)
    for i, row in enumerate(grid):
        for j, n in enumerate(row):
            if n:
                select(X, Y, (i, j, n))
    for solution in solve(X, Y, []):
        for (r, c, n) in solution:
            grid[r][c] = n
        yield grid

def exact_cover(X, Y):
    X = {j: set() for j in X}
    for i, row in Y.items():
        for j in row:
            X[j].add(i)
    return X, Y

def solve(X, Y, solution):
    if not X:
        yield list(solution)
    else:
        c = min(X, key=lambda c: len(X[c]))
        for r in list(X[c]):
            solution.append(r)
            cols = select(X, Y, r)
            for s in solve(X, Y, solution):
                yield s
            deselect(X, Y, r, cols)
            solution.pop()

def select(X, Y, r):
    cols = []
    for j in Y[r]:
        for i in X[j]:
            for k in Y[i]:
                if k != j:
                    X[k].remove(i)
        cols.append(X.pop(j))
    return cols

def deselect(X, Y, r, cols):
    for j in reversed(Y[r]):
        X[j] = cols.pop()
        for i in X[j]:
            for k in Y[i]:
                if k != j:
                    X[k].add(i)

def check_solution(grid):

    #check line
    for row in grid:
        if sum(row) != 45:
            return False

    #check column
    for i in range(len(grid)):
        col = column(grid, i)
        if sum(col) != 45:
            return False

    i = 0
    j = 0
    M = [ [] for i in range(9)]
    for row in grid:
        M[i].append(row[:3])
        M[i+1].append(row[3:6])
        M[i+2].append(row[6:9])
        j += 1
        if j%3 == 0:
            i += 3
    for m in M:
        if sum(m) != 45:
            return False
    return True

def column(matrix, i):
    return [row[i] for row in matrix]
