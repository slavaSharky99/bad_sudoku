from flask import Blueprint, render_template, jsonify, request
from back.sudoku import Grid, null_grid, check_solution
import json

urls = Blueprint('urls', __name__,)

@urls.route('/test')
def test():
    return "This is test Page"

@urls.route('/test_react')
def test_react():
    return render_template("index.html")



@urls.route('/get_grid', methods=["GET"])
def get_grid():

    grid = Grid()
    grid.mix()

    grid = null_grid(grid)
    print(grid.show())
    return jsonify({"table":grid.table})

@urls.route('/post_grid', methods=["POST"])
def post_grid():
    grid = request.json['grid']

    b = check_solution(grid)
    if b:
        return jsonify({"status":"Ok"})
    else:
        return jsonify({"status":"Bad"})
