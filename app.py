from flask import Flask
from back.views import urls

app = Flask(__name__, template_folder='front/template',
                      static_folder='front/static')
app.register_blueprint(urls)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
